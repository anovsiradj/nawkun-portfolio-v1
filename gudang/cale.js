/*
Scale Function
@version 201509061954 (created)
@author Mayendra Costanov <anov.siradj22@gmail.com>
*/
function scale(opt) {
	var n = $.extend({
		w1:null,
		h1:null,
		w2:4,
		h2:3
	},opt);
	var r = 0;
	if (n.w1 !== null) {
		r = (n.w1*n.h2)/n.w2;
	} else {
		r = (n.h1*n.w2)/n.h2;
	};
	return r;
}
